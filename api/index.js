const dotenv = require('dotenv');
const Koa = require('koa');
const Router = require('@koa/router');
const cors = require('@koa/cors');
const axios = require('axios');

dotenv.config();

const app = new Koa();
const router = new Router();

const SHOPIFY_API = process.env.SHOPIFY_API;
const SHOPIFY_TOKEN = process.env.SHOPIFY_TOKEN;
const SERVER_PORT = process.env.NODE_PORT || 4000;

router.get('/products', async (ctx) => {
  const config = {
    methdo: 'get',
    url: SHOPIFY_API,
    params: ctx.query, // append request query to shopify directly
    headers: {
      'X-Shopify-Access-Token': SHOPIFY_TOKEN,
    },
  };

  try {
    const response = await axios(config);
    ctx.body = response.data;
  } catch (error) {
    ctx.throw(500, error.message);
  };
});

app
  .use(cors())
  .use(router.routes())
  .use(router.allowedMethods());

app.listen(SERVER_PORT);
console.log(`Listening to port ${SERVER_PORT}`);