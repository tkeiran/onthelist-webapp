# Backend API

This API is a demo API which wraps Shopify Product API.

## How to start

To start developement:
```shell
cd ./api
yarn install
# start development mode
yarn dev
```

To start production:
```shell
# optional
export NODE_ENV=production
export NODE_PORT=80

# start server
cd ./api
yarn install
yarn start
```