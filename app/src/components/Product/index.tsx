import { createStyles, Grid, makeStyles, Typography } from '@material-ui/core';

const useStyles = makeStyles(() => 
  createStyles({
    productImage: {
      width: '100%',
      maxWidth: '100%',
    },
    image: {
      width: 'auto',
      height: 'auto',
      maxWidth: '100%',
    },
    productPrice: {
      fontSize: '1.4em',
      fontWeight: 'bold',
    }
  }),
);

export type ImageProps = {
  id: string,
  product_id: string,
  alt: string,
  width: number,
  height: number,
  src: string,
}

export type VariantProps = {
  id: string,
  product_id: string,
  title: string,
  price: number,
  compare_at_price: number,
  sku: string,
};

export type ProductProps = {
  id: string,
  title: string,
  status: string,
  image: ImageProps,
  variants: VariantProps[],
};

const Product = ({
  title: name,
  image,
  variants,
}:ProductProps): JSX.Element => {
  const classes = useStyles();

  const {minPrice, maxPrice} = variants.reduce((acc, curr):{minPrice:number, maxPrice:number} => {
    const prices = {
      minPrice: Math.min(curr.price, acc.minPrice || curr.price),
      maxPrice: Math.max(curr.price, acc.maxPrice || curr.price),
    };
    return prices;
  }, {minPrice: NaN, maxPrice: NaN});

  return (
    <Grid item xs={12} sm={6} md={4}>
      <div className={classes.productImage}>
        <img className={classes.image} src={image.src} alt={image.alt} />
      </div>
      <Typography>{name}</Typography>
      {minPrice===maxPrice ? (
        <Typography className={classes.productPrice}>{`$${minPrice}`}</Typography>
      ): (
        <Typography className={classes.productPrice}>{`$${minPrice}-${maxPrice}`}</Typography>
      )}
    </Grid>
  )
};

export default Product;