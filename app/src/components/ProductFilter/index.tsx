import { ChangeEvent } from 'react';
import { createStyles, FormControl, Grid, Link, makeStyles, MenuItem, Select } from '@material-ui/core';

const useStyles = makeStyles(() => 
  createStyles({
    link: {
      color: '#000',
      textDecoration: 'none',
    },
    activeLink: {
      color: '#000',
      borderBottom: '2px solid #000',
      textDecoration: 'none',
    },
  }),
);

export type ProductSortOptionsProps = {
  value: string,
  label: string,
};

export type ProductFilterProps = {
  currentSort: string,
  handleChange: (event:ChangeEvent<{ value:unknown }>) => void,
  sortOptions: ProductSortOptionsProps[],
  currentView: number,
  handleView: (option:number) => void,
  viewOptions: number[],
};

const ProductFilter = ({
  currentSort,
  handleChange,
  sortOptions,
  currentView,
  handleView,
  viewOptions,
}:ProductFilterProps):JSX.Element => {
  const classes = useStyles();

  return (
    <>
      <Grid item>SORT BY</Grid>
      <FormControl>
        <Select
          id="sort-select"
          value={currentSort}
          onChange={handleChange}
        >
           {sortOptions.map((opt, index) => (<MenuItem key={index} value={opt.value}>{opt.label}</MenuItem>))}
        </Select>
      </FormControl>
      <Grid item>VIEWING</Grid>
      {viewOptions.map((option, index) => (
        <Grid item key={index}>
          <Link href="#" onClick={() => handleView(option)} className={currentView===option ? classes.activeLink : classes.link}>{option}</Link>
        </Grid>
      ))}
    </>
  );
};

export default ProductFilter;