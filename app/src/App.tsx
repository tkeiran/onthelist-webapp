import React, { ChangeEvent, useEffect, useState } from 'react';
import { Container, createStyles, CssBaseline, Grid, makeStyles } from '@material-ui/core';
import useAxios from 'axios-hooks';
import Product, { ProductProps } from './components/Product';
import ProductFilter from './components/ProductFilter';
import { useLocation } from 'react-router';

const API_ENDPOINT = process.env.REACT_APP_API || '';

const useStyles = makeStyles(() => 
  createStyles({
    root: {
      flexGrow: 1,
      padding: '15px 30px',
    },
    productFilter: {
      marginBottom: '20px',
    }
  }),
);

export default function App() {
  const classes = useStyles();
  const { search } = useLocation();
  const searchParams = search
    .replace('?', '')
    .split('&')
    .map((item) => item.split('='));

  const getURLQuery = (key:string) => {
    return searchParams.find(s => s[0] === key)?.pop();
  };

  const [products, setProducts] = useState([]);
  const [currentSort, setCurrentSort] = useState(getURLQuery('order') || 'id asc');
  const [currentView, setCurrentView] = useState(parseInt(getURLQuery('offset') || '12'));

  const sortOptions = [
    {label: 'Default', value: 'id asc'},
    {label: 'Name: A to Z', value: 'title asc'},
    {label: 'Name: Z to A', value: 'title desc'},
    {label: 'Latest Products', value: 'published_at desc'},
  ];

  const handleChange = (event:ChangeEvent<{ value:unknown }>) => {
    setCurrentSort(event.target.value as string);
  }

  const viewOptions = [12, 24, 48, 96];

  const handleView = (option:number) => {
    setCurrentView(option);
  }

  // get products via axios hook
  const [{ data, loading, error }, refetch] = useAxios(`${API_ENDPOINT}/products`);

  useEffect(() => {
    setProducts(data?.products || []);
  }, [data]);

  useEffect(() => {
    refetch({
      params: {
        order: currentSort,
        limit: currentView,
      }
    });
  }, [refetch, currentSort, currentView]);


  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="lg" className={classes.root}>
        <Grid container className={classes.productFilter} spacing={3} justify="flex-end" alignItems="baseline">
          <ProductFilter
            currentSort={currentSort}
            sortOptions={sortOptions}
            handleChange={handleChange}
            currentView={currentView}
            viewOptions={viewOptions}
            handleView={handleView}
          />
        </Grid>
        {loading && <h1>loading...</h1>}
        {error && <h1>Error occurs</h1>}
        {!loading && !error && (
          <Grid container spacing={3} alignContent="flex-end">
            {products.map((product:ProductProps, index) => (<Product key={index} {...product} />))}
          </Grid>
        )}
      </Container>
    </React.Fragment>
  );
}
