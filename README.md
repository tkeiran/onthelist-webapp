# OnTheList Technical Test

This is a technical test from OnTheList. This project consists of 2 parts - frontend app and backend API service.

To make this app better scalability and flexibility, I decouple the backend API and frontend React app.

## How to start development

1. Start API server in dev mode

```shell
cd ./api
yarn install
yarn dev
```

2. Start React app in dev mode

```shell
cd ./app
yarn install
yarn start
```

## How to build docker image

### Build backend API service

```shell
# docker build -t <image name> -f Dockerfile.api .
# e.g.
docker build -t onthelist-api -f Dockerfile.api .

# start the docker image locally
docker run --rm -d  -p 4000:4000 onthelist-api
```
### Build frontend react app and serve static HTML by Nginx

```shell
# docker build -t <image name> -f Dockerfile.app .
# e.g.
docker build -t onthelist-app -f Dockerfile.app .

# start the docker image locally
docker run --rm -d  -p 3000:80 onthelist-app
# open url: http://localhost:3000
```
